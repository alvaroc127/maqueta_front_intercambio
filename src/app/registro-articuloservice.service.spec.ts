import { TestBed } from '@angular/core/testing';

import { RegistroArticuloserviceService } from './registro-articuloservice.service';

describe('RegistroArticuloserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegistroArticuloserviceService = TestBed.get(RegistroArticuloserviceService);
    expect(service).toBeTruthy();
  });
});
