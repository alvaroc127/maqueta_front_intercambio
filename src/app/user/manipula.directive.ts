import { Directive,HostListener,ElementRef,OnInit,OnDestroy } from '@angular/core';

@Directive({
  selector: '[appManipula]'
})
export class ManipulaDirective implements OnInit,OnDestroy {

  constructor(private element:ElementRef) { }

  

    ngOnInit() {
    this.element.nativeElement.addEventListener('click', this.obtenerid);
		}

  	ngOnDestroy() {
  	 this.element.nativeElement.removeEventListener('click', this.obtenerid);
  	}

  @HostListener('click',['$event.target.id'])
  obtenerid(id:string) 
  	{
  	 console.log(id);
  	   localStorage.setItem('buttonid',id);
	}
}
