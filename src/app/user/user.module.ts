import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UserRoutingModule } from './user-routing.module';
import { MisarticuloscomponentComponent } from './misarticuloscomponent/misarticuloscomponent.component';
import { ArticulosdisponiblescomponentComponent } from './articulosdisponiblescomponent/articulosdisponiblescomponent.component';
import { MissolicitudescomponentComponent } from './missolicitudescomponent/missolicitudescomponent.component';
import { PerfilcomponentComponent } from './perfilcomponent/perfilcomponent.component';
import { RegistroaritucComponentComponent } from './registroarituc-component/registroarituc-component.component';
import { ManipulaDirective } from './manipula.directive';

@NgModule({
  declarations: [MisarticuloscomponentComponent, ArticulosdisponiblescomponentComponent, MissolicitudescomponentComponent, PerfilcomponentComponent, RegistroaritucComponentComponent, ManipulaDirective],
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,
  ]
})
export class UserModule { }
