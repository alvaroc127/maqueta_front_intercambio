import { Component, OnInit } from '@angular/core';
import { Solicitud} from '../../solicitud';
import {ServicioIntercambioService} from '../../servicio-intercambio.service';
import {UsuarioserviceService} from '../../usuarioservice.service';

@Component({
  selector: 'app-missolicitudescomponent',
  templateUrl: './missolicitudescomponent.component.html',
  styleUrls: ['./missolicitudescomponent.component.css']
})
export class MissolicitudescomponentComponent implements OnInit {
	 solicitudes:Solicitud[]=[]

  constructor(private servicioUsuario:UsuarioserviceService,
  			private servicioIntercamb:ServicioIntercambioService) { }

  ngOnInit() {
  		let use= this.servicioUsuario.getUserLoggedIn();
  		this.servicioIntercamb.consultarMissolitudes(use).subscribe(
  			resp=>
  			{
  				console.log(resp.mensaje);
  				console.log(resp.id);
  				if(null !=  resp.data)
  				{
  					this.solicitudes=JSON.parse(resp.data);
  				}
  			},
  			error=>{
  				console.log("Error en el procesamiento");


  			}

  		);

  }



}
