import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissolicitudescomponentComponent } from './missolicitudescomponent.component';

describe('MissolicitudescomponentComponent', () => {
  let component: MissolicitudescomponentComponent;
  let fixture: ComponentFixture<MissolicitudescomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissolicitudescomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissolicitudescomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
