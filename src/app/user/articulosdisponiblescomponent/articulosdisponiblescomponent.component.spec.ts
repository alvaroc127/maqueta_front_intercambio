import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulosdisponiblescomponentComponent } from './articulosdisponiblescomponent.component';

describe('ArticulosdisponiblescomponentComponent', () => {
  let component: ArticulosdisponiblescomponentComponent;
  let fixture: ComponentFixture<ArticulosdisponiblescomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticulosdisponiblescomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticulosdisponiblescomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
