import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilcomponentComponent } from './perfilcomponent.component';

describe('PerfilcomponentComponent', () => {
  let component: PerfilcomponentComponent;
  let fixture: ComponentFixture<PerfilcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
