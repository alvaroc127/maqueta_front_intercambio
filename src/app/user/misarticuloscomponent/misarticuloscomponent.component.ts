import { Component, OnInit,HostListener } from '@angular/core';
import {Articulo} from '../../articulo';
import {NgForm} from '@angular/forms';
import {ServicioIntercambioService} from '../../servicio-intercambio.service';
import {UsuarioserviceService} from '../../usuarioservice.service';
import {Solicitud} from '../../solicitud';

@Component({
  selector: 'app-misarticuloscomponent',
  templateUrl: './misarticuloscomponent.component.html',
  styleUrls: ['./misarticuloscomponent.component.css']
})
export class MisarticuloscomponentComponent implements OnInit {
	 articulos:Articulo[]=[];
	 aritulosdisp:Articulo[]=[];
   articuSelec1:Articulo=new Articulo;
	 articuSelec2:Articulo=new Articulo;

  constructor(private servicioIntercambio:ServicioIntercambioService,
  			 private servicioUsuario:UsuarioserviceService) { }

  ngOnInit(){
  	this.servicioIntercambio.consultaArticulosUsuario(this.servicioUsuario.getUserLoggedIn()).
  	subscribe(
  		res=>{
  			if(null != res.data){
  				this.articulos= JSON.parse(res.data);
  			}

  		},
  		error=>{
  				console.log("error con la consulta de usuario");
  		}

  		);
  	this.servicioIntercambio.consultarArticulosDisponibles()
  	.subscribe(
  		resp=>{
  			if(null != resp.data){
  				this.aritulosdisp =JSON.parse(resp.data);
  			}
  		},
  		error=>{
  				console.log("error con los articulos");
  			}
  		);

  }

  evento(){
  	let id=parseInt(localStorage.getItem('buttonid'));
  	console.log(id);
  	this.articuSelec2=this.aritulosdisp.find( function(art){
		return art.arIderegistro ==  id;
  	});
  }

  carga(){
  	let id:number=parseInt(localStorage.getItem('buttonid'));
  	console.log(id);
  	this.articuSelec1= this.aritulosdisp.find( function(art){
		return art.arIderegistro ==  id;
  	});
  	console.log("secarga el objeto 1");
  	console.log(this.articuSelec1);
  }

intercambiar(){
	console.log(this.articuSelec1);
	console.log(this.articuSelec2);
	var solicitud:Solicitud=new Solicitud
	solicitud.arIdArticuloRemitente=this.articuSelec1;
	solicitud.arIdArticuloDestinatario=this.articuSelec2;
	solicitud.soObservacion="se le debe la capturada de la obervacion"
	console.log(solicitud)
	this.servicioIntercambio.intercambiar(solicitud).
	subscribe(
		art=>{
			console.log(art.id);
			console.log(art.mensaje);
			if(null!=art.data && art.id=='200')
			{
				alert("se proceso de forma correcta");
			}
		},
		error=>{
			console.log("se proceso de forma incorrecta");
			}
		);
	}

}
