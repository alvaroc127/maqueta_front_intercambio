import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisarticuloscomponentComponent } from './misarticuloscomponent.component';

describe('MisarticuloscomponentComponent', () => {
  let component: MisarticuloscomponentComponent;
  let fixture: ComponentFixture<MisarticuloscomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisarticuloscomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisarticuloscomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
