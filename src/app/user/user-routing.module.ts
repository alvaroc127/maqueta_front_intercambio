import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PerfilcomponentComponent} from './perfilcomponent/perfilcomponent.component'; 
import {MisarticuloscomponentComponent} from './misarticuloscomponent/misarticuloscomponent.component';
import {ArticulosdisponiblescomponentComponent} from './articulosdisponiblescomponent/articulosdisponiblescomponent.component';
import {MissolicitudescomponentComponent} from './missolicitudescomponent/missolicitudescomponent.component';
import {RegistroaritucComponentComponent} from './registroarituc-component/registroarituc-component.component';

const routes: Routes = 
[
	
	{
		path:'',
		component:PerfilcomponentComponent
	},
	{
		path:'misarticulos',
		component:MisarticuloscomponentComponent

	},
	{
		path:'articulosdisponibles',
		component:ArticulosdisponiblescomponentComponent
	},
	{
		path:'solicitudes',
		component:MissolicitudescomponentComponent
	},
	{
		path:'registroarticulo',
		component:RegistroaritucComponentComponent
	},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
