import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { UsuarioserviceService } from '../../usuarioservice.service';
import {RegistroArticuloserviceService} from '../../registro-articuloservice.service'
import { Articulo } from '../../articulo';


@Component({
  selector: 'app-registroarituc-component',
  templateUrl: './registroarituc-component.component.html',
  styleUrls: ['./registroarituc-component.component.css']
})
export class RegistroaritucComponentComponent implements OnInit {
	  articulo:Articulo=new Articulo
	  tipos:string[];
	  selec:any="C";
	  eliminar:boolean=false;

  constructor(private usuarioServicio:UsuarioserviceService,
  	private registroArticulo:RegistroArticuloserviceService) { }

  ngOnInit() {
  	this.registroArticulo.tipoArticulo().
  	subscribe(
  		resp=>
  			{
  				if(resp.id == '200')
  				{
  					this.tipos=JSON.parse(resp.data);
  				}
  			},
  		error=>
  			{
  				console.log("ALGO FALLO CONSULTA TIPOS");

  			}
  		);
  }


  recepcionForm(articulo:NgForm)
  {	
  	var articuloentrada:Articulo=
  	{
  	arIderegistro:null,
	arNombre:articulo.value.arNombre,
	arTip: articulo.value.selec=="HOGARES"?2:1,
	arFoto:articulo.value.arFoto,
	cliIdCliente:null,
  	}
  	if(this.eliminar){
  		var articuloconsul:Articulo=null;
  		this.registroArticulo.buscarArticulo(articuloentrada).
  			subscribe(
  		resp=>
  		{
  			alert(resp.mensaje);
  			if(null != resp.data)
  			{
  				articuloconsul=JSON.parse(resp.data);
  				this.registroArticulo.
  					eliminarArticulo(articuloconsul).subscribe(
  				res=>{
  					alert(res.mensaje);
  			    },
  			    error=>{
  					console.log("ERROR ELIMINANDO");
  					}
  				);

  			}
  		},
  		error=>
  			{
  			console.log("error en articulo");
  			}	
  		);
  		
   }

  	if(!this.eliminar){
  	console.log(articuloentrada);
  	var articuloconsul:Articulo=null;
  	articuloentrada.cliIdCliente=this.usuarioServicio.getUserLoggedIn().cliIdecliente;
  	console.log(articuloentrada.cliIdCliente);
  	this.registroArticulo.buscarArticulo(articuloentrada).
  	subscribe(
  		resp=>
  		{
  			if(null != resp.data){
  				articuloconsul=JSON.parse(resp.data);
	  			}
  		},
  		error=>
  			{
  			console.log(error);
  			console.log("error en articulo");
  			}	
  		);
  		if( null !=articuloconsul){
  			articuloentrada.arIderegistro=articuloconsul.arIderegistro
  		}
  		this.registroArticulo.guardarModificar(articuloentrada).subscribe
  			(
  				res=>{
  					console.log(res.id);
  					console.log(res.mensaje);
  					alert(res.mensaje);
  				},error=>
  				{
  					console.log("error en el guardar");
  				}
  			);
  	}
  	this.eliminar=false;
  }





}
