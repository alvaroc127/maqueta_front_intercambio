import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RegistroaritucComponentComponent } from './registroarituc-component.component';

describe('RegistroaritucComponentComponent', () => {
  let component: RegistroaritucComponentComponent;
  let fixture: ComponentFixture<RegistroaritucComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroaritucComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroaritucComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
