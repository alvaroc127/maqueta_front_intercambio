import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponentComponent} from './login-component/login-component.component';
import {RegistroComponentComponent} from './registro-component/registro-component.component';

const routes: Routes = 
[   
	{path:'',
	loadChildren:'./home/home.module#HomeModule'
	},
	{
		path:'login',
		component:LoginComponentComponent
	},
	{
		path:'registro',
		component:RegistroComponentComponent
	},
	{
		path:'user',
		loadChildren:'./user/user.module#UserModule'

	}
	,
	{
		path:'**',
		redirectTo:''
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
