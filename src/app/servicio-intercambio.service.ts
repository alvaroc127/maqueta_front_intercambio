import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {Articulo} from './articulo';
import {Usuario} from './usuario';
import {Respuesta} from './respuesta';
import {Solicitud} from './solicitud';


@Injectable({
  providedIn: 'root'
})

export class ServicioIntercambioService {
  constructor(private http:HttpClient) { }



  consultaArticulosUsuario(usuario:Usuario)
  { 
  	return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/intercambio/consultar/misarticulos',
  		usuario);
  }

  consultarArticulosDisponibles(){

  	return this.http.post<Respuesta>(
  		'https://intercambioback.herokuapp.com/api/intercambio/consutlar/articulos',{}
  		);
  }


  intercambiar(solicitud:Solicitud){
  	return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/solicitar/articulo/',
  		solicitud);

  }

  consultarMissolitudes(usuario:Usuario)
  {
    return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/intercambio/consutlar/solicitudes',usuario);

  }
}
