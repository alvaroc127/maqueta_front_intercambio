import { Component, OnInit} from '@angular/core';
import {UsuarioserviceService} from '../usuarioservice.service';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit{
	public isLogged:boolean;

  constructor(private servicioUsuari:UsuarioserviceService) { 
  }

  ngOnInit() 
  	{
  	this.servicioUsuari.estalogeado.subscribe( bandera => this.setIslogged(bandera));
  	if(null === this.servicioUsuari.getUserLoggedIn())
  	{
  		this.isLogged=false;
  	}else{
  		this.isLogged=true;
  	}
  }


  logout()
  {
  	this.servicioUsuari.salidaUser();
  	this.isLogged=false;
  }

  setIslogged(banderacambio:boolean)
  {
  	this.isLogged=banderacambio;
  }

  




}
