import { Component, OnInit } from '@angular/core';
import { UsuarioserviceService } from '../usuarioservice.service';
import {LoginserviceService } from '../loginservice.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Usuario} from '../usuario';
import {Cliente} from '../cliente';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})

export class LoginComponentComponent implements OnInit {
	public usuario:Usuario=
	{
		usCorreo:'',
		usPassword:'',
		usNombre: '',
		usRePassword: '',
		cliIdecliente:new Cliente
	}

  constructor
  (private usuarioService:UsuarioserviceService,private loginservice:LoginserviceService,private route:Router){   }

  ngOnInit() {
  }

  login(user:NgForm){
  	console.log(user.value);
  	this.loginservice.login(user.value).subscribe(
  		resp=>{
  			let user1:Usuario= JSON.parse(resp.data);
  			if(null!= user1){
  			this.usuarioService.setusserLogged(user1);
  			this.route.navigateByUrl('user');
  			}
  		},
  		error=>{
  			console.log("error");
  		}
  	);
  }

}