import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Usuario } from './usuario';
import { Respuesta } from './respuesta';

@Injectable({
	providedIn: 'root'
})
export class LoginserviceService {

	constructor(private http: HttpClient) { }

	login(usuario: Usuario) {
		return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/autenticar/user/',
			{
				usPassword: usuario.usPassword,
				usCorreo: usuario.usCorreo
			});


	}

	registro(usuario: Usuario) {
		return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/registro/usuario/',
			{
				usNombre: usuario.usNombre,
				usPassword: usuario.usPassword,
				usCorreo: usuario.usCorreo
			});
	}
}
