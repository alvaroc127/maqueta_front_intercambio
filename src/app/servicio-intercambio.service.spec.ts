import { TestBed } from '@angular/core/testing';

import { ServicioIntercambioService } from './servicio-intercambio.service';

describe('ServicioIntercambioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicioIntercambioService = TestBed.get(ServicioIntercambioService);
    expect(service).toBeTruthy();
  });
});
