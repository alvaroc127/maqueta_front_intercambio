import { TestBed } from '@angular/core/testing';

import { UsuarioserviceService } from './usuarioservice.service';

describe('UsuarioserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsuarioserviceService = TestBed.get(UsuarioserviceService);
    expect(service).toBeTruthy();
  });
});
