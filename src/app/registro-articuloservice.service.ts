import { Injectable } from '@angular/core';
import {Articulo} from  './articulo';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {Respuesta} from './respuesta';


@Injectable({
  providedIn: 'root'
})
export class RegistroArticuloserviceService {

  constructor(private http:HttpClient) { }

  guardarModificar(articulo:Articulo)
  {
  	console.log("en el guardar");
  	console.log(articulo);
  	return this.http.post<Respuesta>(
  		'https://intercambioback.herokuapp.com/api/intercambio/modificar/otro/'
  		,articulo);
  }


  buscarArticulo(articulo:Articulo){
  	console.log("enel buscar");
  	console.log(articulo);
  	return this.http.post<Respuesta>(
  		'https://intercambioback.herokuapp.com/api/intercambio/consultar/otro/',
  		articulo);
  }

  tipoArticulo(){
  	return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/intercambio/consulta/tipoarticulo',{});  //servicio para consultar los tipos
  }
  

  eliminarArticulo(articulo:Articulo){
  	return this.http.post<Respuesta>('https://intercambioback.herokuapp.com/api/intercambio/eliminar/articulo',articulo); //eliminar el articulo selecciona
  }



}
