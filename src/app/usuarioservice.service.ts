import { Injectable, Output,EventEmitter} from '@angular/core';
import {Usuario} from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioserviceService {

  @Output() estalogeado:EventEmitter<any> =new EventEmitter();
	
	private  usserLogged:Usuario;



  setusserLogged(usuario:Usuario){
    this.estalogeado.emit(true);
  	this.usserLogged = usuario;
  	localStorage.setItem
  	('llaveusuario',JSON.stringify(usuario));
  }

  getUserLoggedIn():Usuario
  {
  		return JSON.parse(localStorage.getItem('llaveusuario'));
  }

  salidaUser()
  {
    localStorage.removeItem('llaveusuario');
    this.usserLogged=null;
    this.estalogeado.emit(false);
  }



}
