import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsuarioserviceService } from '../usuarioservice.service';
import {LoginserviceService } from '../loginservice.service';
import {Router} from '@angular/router';
import { NgForm } from '@angular/forms';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-registro-component',
  templateUrl: './registro-component.component.html',
  styleUrls: ['./registro-component.component.css']
})

export class RegistroComponentComponent implements OnInit {
  @ViewChild('alert') alert: ElementRef;
  public usuario: Usuario =
    {
      usCorreo: '',
      usPassword: '',
      usNombre: '',
      usRePassword: '',
      cliIdecliente: null
    }

  constructor
    (private usuarioService:UsuarioserviceService,private loginservice:LoginserviceService,private route:Router){   }

  ngOnInit() {
  }

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }

  registro(user: NgForm) {
    let usuario: Usuario = user.value;
    if (usuario.usPassword == usuario.usRePassword) {
      this.loginservice.registro(user.value).subscribe(
        resp => {
          console.log(resp);
          if (resp.mensaje == 'Ok') {
            this.alert.nativeElement.classList.remove('alert-danger');
            this.alert.nativeElement.classList.add('alert-success');
            this.alert.nativeElement.classList.add('show');

            this.usuarioService.setusserLogged(usuario);
  			    this.route.navigateByUrl('user');
          }
          else {
            this.alert.nativeElement.classList.remove('alert-success');
            this.alert.nativeElement.classList.add('alert-danger');
            this.alert.nativeElement.classList.add('show');
          }
        },
        error => {
          console.log("Fallido");
          this.alert.nativeElement.classList.remove('alert-success');
          this.alert.nativeElement.classList.add('alert-danger');
          this.alert.nativeElement.classList.add('show');
        });
    }
    else {
      this.alert.nativeElement.classList.remove('alert-success');
      this.alert.nativeElement.classList.add('alert-danger');
      this.alert.nativeElement.classList.add('show');
    }
  }
}
