import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuienesomosComponentComponent} from './quienesomos-component/quienesomos-component.component';
import {IndexComponentComponent} from './index-component/index-component.component';

const routes: Routes = 
[
	{path:'quienes-somos',
	component:QuienesomosComponentComponent
	},
	{
		path:'',
		component:IndexComponentComponent
	},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
