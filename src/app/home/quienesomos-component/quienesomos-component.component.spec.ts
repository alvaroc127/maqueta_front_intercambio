import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuienesomosComponentComponent } from './quienesomos-component.component';

describe('QuienesomosComponentComponent', () => {
  let component: QuienesomosComponentComponent;
  let fixture: ComponentFixture<QuienesomosComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuienesomosComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuienesomosComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
