import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { QuienesomosComponentComponent } from './quienesomos-component/quienesomos-component.component';
import { IndexComponentComponent } from './index-component/index-component.component';

@NgModule({
  declarations: [QuienesomosComponentComponent, IndexComponentComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
