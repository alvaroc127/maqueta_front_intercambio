import { Articulo } from './articulo';

export class Solicitud
{
	soCodigo:string;
	soIderegistro:number;
    soTipo:number;
    soEstado:string;
    soObservacion:string;
    arIdArticuloRemitente:Articulo;
	arIdArticuloDestinatario:Articulo;


}