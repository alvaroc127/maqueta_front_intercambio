(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-user-module"],{

/***/ "./src/app/articulo.ts":
/*!*****************************!*\
  !*** ./src/app/articulo.ts ***!
  \*****************************/
/*! exports provided: Articulo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Articulo", function() { return Articulo; });
var Articulo = /** @class */ (function () {
    function Articulo() {
    }
    return Articulo;
}());



/***/ }),

/***/ "./src/app/registro-articuloservice.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/registro-articuloservice.service.ts ***!
  \*****************************************************/
/*! exports provided: RegistroArticuloserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroArticuloserviceService", function() { return RegistroArticuloserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var RegistroArticuloserviceService = /** @class */ (function () {
    function RegistroArticuloserviceService(http) {
        this.http = http;
    }
    RegistroArticuloserviceService.prototype.guardarModificar = function (articulo) {
        console.log("en el guardar");
        console.log(articulo);
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/modificar/otro/', articulo);
    };
    RegistroArticuloserviceService.prototype.buscarArticulo = function (articulo) {
        console.log("enel buscar");
        console.log(articulo);
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/consultar/otro/', articulo);
    };
    RegistroArticuloserviceService.prototype.tipoArticulo = function () {
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/consulta/tipoarticulo', {}); //servicio para consultar los tipos
    };
    RegistroArticuloserviceService.prototype.eliminarArticulo = function (articulo) {
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/eliminar/articulo', articulo); //eliminar el articulo selecciona
    };
    RegistroArticuloserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RegistroArticuloserviceService);
    return RegistroArticuloserviceService;
}());



/***/ }),

/***/ "./src/app/servicio-intercambio.service.ts":
/*!*************************************************!*\
  !*** ./src/app/servicio-intercambio.service.ts ***!
  \*************************************************/
/*! exports provided: ServicioIntercambioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicioIntercambioService", function() { return ServicioIntercambioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var ServicioIntercambioService = /** @class */ (function () {
    function ServicioIntercambioService(http) {
        this.http = http;
    }
    ServicioIntercambioService.prototype.consultaArticulosUsuario = function (usuario) {
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/consultar/misarticulos', usuario);
    };
    ServicioIntercambioService.prototype.consultarArticulosDisponibles = function () {
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/consutlar/articulos', {});
    };
    ServicioIntercambioService.prototype.intercambiar = function (solicitud) {
        return this.http.post('https://intercambioback.herokuapp.com/api/solicitar/articulo/', solicitud);
    };
    ServicioIntercambioService.prototype.consultarMissolitudes = function (usuario) {
        return this.http.post('https://intercambioback.herokuapp.com/api/intercambio/consutlar/solicitudes', usuario);
    };
    ServicioIntercambioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ServicioIntercambioService);
    return ServicioIntercambioService;
}());



/***/ }),

/***/ "./src/app/solicitud.ts":
/*!******************************!*\
  !*** ./src/app/solicitud.ts ***!
  \******************************/
/*! exports provided: Solicitud */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Solicitud", function() { return Solicitud; });
var Solicitud = /** @class */ (function () {
    function Solicitud() {
    }
    return Solicitud;
}());



/***/ }),

/***/ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvYXJ0aWN1bG9zZGlzcG9uaWJsZXNjb21wb25lbnQvYXJ0aWN1bG9zZGlzcG9uaWJsZXNjb21wb25lbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <section class=\"card-columns\" *ngIf=\"articulos.length > 0\">        \r\n    <aside class=\"card\" *ngFor=\"let articulo of articulos\">\r\n            <header class=\"card-header\">\r\n                {{articulo.arNombre}}\r\n                <figure>\r\n                    <img class=\"card-img-top img-fluid\" src={{articulo.arFoto}}>\r\n                </figure>\r\n            </header>\r\n            <div class=\"card-body\">\r\n                <h3 class=\"card-title\">\r\n                    {{articulo.arNombre}}\r\n                </h3>\r\n                <p class=\"card-text\">\r\n                    Tipo articulo\r\n                    {{articulo.arIderegistro}}\r\n                </p>\r\n                <button appManipula (click)=\"carga()\" id={{articulo.arIderegistro}}  class=\"btn btn-primary stretched-link\" data-toggle=\"modal\"\r\n                    data-target=\"#detalle\">\r\n                    Intercambiar\r\n                </button>\r\n            </div>\r\n    </aside>\r\n    </section>\r\n\r\n    <div class=\"modal bd-example-modal-lg\" id=\"detalle\">\r\n        <div class=\"modal-dialog modal-lg\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <h4 class=\"modal-title\">{{articuSelec1.arNombre}}</h4>\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <br>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-12\" style=\"text-align: center;\" >\r\n                            <figure>\r\n                                <img class=\"img-fluid\" src=\"{{articuSelec1.arFoto}}\">\r\n                                <figcaption>\r\n                                </figcaption>\r\n                            </figure>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-1\"> </div>\r\n                        <div class=\"col-sm-10\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-12\">\r\n                                    <!--Adicionar en este label el resumen del articulo-->\r\n                                    <label id=\"resumen\">\r\n                                        \r\n                                    </label>\r\n                                </div>\r\n                            </div>\r\n                            <button type=\"submit\" class=\"btn btn-primary btn-block\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#intercambiar\">Intercambiar</button>\r\n                        </div>\r\n                        <div class=\"col-sm-1\"> </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"modal bd-example-modal-lg\" id=\"intercambiar\">\r\n        <div class=\"modal-dialog modal-lg\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <h4 class=\"modal-title\">Intercambio</h4>\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-6\">\r\n                            <ul class=\"nav nav-tabs nav-justified\" role=\"tablist\">\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#articulos\">Mi\r\n                                        Art&iacute;culo</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#recepcion\">Recepci&oacute;n</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#resum\">Resumen</a>\r\n                                </li>\r\n                            </ul>\r\n                            <div class=\"tab-content\">\r\n                                <div class=\"tab-pane show active\" id=\"articulos\" role=\"tabpanel\">\r\n                                    <br>\r\n                                    <section class=\"card-columns\" *ngIf=\"aritulosdisp.length > 0\">\r\n                                        <aside class=\"card\" *ngFor=\"let arti of aritulosdisp;\r\n                                         let in=index\">\r\n                                            <header class=\"card-header\">\r\n                                                {{arti.arNombre}}\r\n                                                <figure>\r\n                                                    <span> {{arti.arIderegistro}}</span>\r\n                                                    <img class=\"card-img-top img-fluid\"\r\n                                                        src=\"{{arti.arFoto}}\">\r\n                                                    <figcaption>\r\n                                                    </figcaption>\r\n                                                        <span> {{arti.arTip}}</span>\r\n                                                </figure>\r\n                                            </header>\r\n                                            <button appManipula id=\"{{arti.arIderegistro}}\" class=\"btn btn-primary\"  (click)=\"evento()\" > {{arti.arIderegistro}}</button>\r\n                                                  <!--GENERAR UN EVENTO EN EL LCIK-->\r\n                                        </aside>\r\n                                    </section>\r\n                                </div>\r\n                                <div class=\"tab-pane\" id=\"recepcion\" role=\"tabpanel\">\r\n                                    <br>\r\n                                    <form action=\"\" onsubmit=\"\">\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-sm-1\"> </div>\r\n                                            <div class=\"col-sm-10\">\r\n                                                <div class=\"row\">\r\n                                                    <div class=\"col-sm-6\">\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" class=\"form-control\"\r\n                                                                placeholder=\"Departamento\" required>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                    <div class=\"col-sm-6\">\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" class=\"form-control\" placeholder=\"Ciudad\"\r\n                                                                required>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div class=\"row\">\r\n                                                    <div class=\"col-sm-12\">\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" class=\"form-control\"\r\n                                                                placeholder=\"Dirección\" required>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div class=\"row\">\r\n                                                    <div class=\"col-sm-12\">\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" class=\"form-control\"\r\n                                                                placeholder=\"Datos Adicionales\" required>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div class=\"row\">\r\n                                                    <div class=\"col-sm-12\">\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" class=\"form-control\" placeholder=\"Barrio\"\r\n                                                                required>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"col-sm-1\"> </div>\r\n                                        </div>\r\n                                    </form>\r\n                                </div>\r\n                                <div class=\"tab-pane\" id=\"resum\" role=\"tabpanel\">\r\n                                    <br>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-sm-1\"> </div>\r\n                                        <div class=\"col-sm-10\">\r\n                                            <div class=\"row\">\r\n                                                <div class=\"col-sm-12\">\r\n                                                    <!--Adicionar en este label el resumen de lo parametrizado en los otros tabs-->\r\n                                                    <label  id=\"resumen\">\r\n                                                    </label>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"row\">\r\n                                                <div class=\"col-sm-12\">\r\n                                                    <div class=\"form-group\">\r\n                                                        <label for=\"comment\"><strong>Información\r\n                                                                Adicional:</strong></label>\r\n                                                        <textarea class=\"form-control\" rows=\"3\" id=\"comment\"></textarea>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"row\">\r\n                                                <div class=\"col-sm-12\">\r\n                                                    <div class=\"form-group form-check\">\r\n                                                        <label class=\"form-check-label\">\r\n                                                            <input type=\"checkbox\" class=\"form-check-input\"\r\n                                                                name=\"forregcheck\" required>\r\n                                                            <a href=\"TerminosCondiciones.html\">Acepto terminos y\r\n                                                                Condiciones</a>\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <button type=\"submit\" (click)=\"intercambiar()\" class=\"btn btn-primary btn-block \">Enviar\r\n                                                Solicitud</button>\r\n                                        </div>\r\n                                        <div class=\"col-sm-1\"> </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-2\"></div>\r\n                        <div class=\"col-4\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-12\">\r\n                                    <figure>\r\n                                        <img class=\"img-fluid\" src=\"{{articuSelec1.arFoto}}\">\r\n                                        <figcaption>\r\n                                            Artículo a dar\r\n                                        </figcaption>\r\n                                    </figure>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-12\" *ngIf=\"null != articuSelec2\">\r\n                                    <figure>\r\n                                        <!--Se debe cargar la imagen del articulo seleccionado en el tab articulos-->\r\n                                        <img class=\"img-fluid\" src=\"{{articuSelec2.arFoto}}\">\r\n                                        <figcaption>\r\n                                            Artículo a recibir\r\n                                        </figcaption>\r\n                                    </figure>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ArticulosdisponiblescomponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArticulosdisponiblescomponentComponent", function() { return ArticulosdisponiblescomponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _articulo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../articulo */ "./src/app/articulo.ts");
/* harmony import */ var _servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicio-intercambio.service */ "./src/app/servicio-intercambio.service.ts");
/* harmony import */ var _usuarioservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../usuarioservice.service */ "./src/app/usuarioservice.service.ts");
/* harmony import */ var _solicitud__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../solicitud */ "./src/app/solicitud.ts");






var ArticulosdisponiblescomponentComponent = /** @class */ (function () {
    function ArticulosdisponiblescomponentComponent(servicioIntercambio, servicioUsuario) {
        this.servicioIntercambio = servicioIntercambio;
        this.servicioUsuario = servicioUsuario;
        this.articulos = [];
        this.aritulosdisp = [];
        this.articuSelec1 = new _articulo__WEBPACK_IMPORTED_MODULE_2__["Articulo"];
        this.articuSelec2 = new _articulo__WEBPACK_IMPORTED_MODULE_2__["Articulo"];
    }
    ArticulosdisponiblescomponentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.servicioIntercambio.consultaArticulosUsuario(this.servicioUsuario.getUserLoggedIn()).
            subscribe(function (res) {
            if (null != res.data) {
                _this.articulos = JSON.parse(res.data);
            }
        }, function (error) {
            console.log("error con la consulta de usuario");
        });
        this.servicioIntercambio.consultarArticulosDisponibles()
            .subscribe(function (resp) {
            if (null != resp.data) {
                _this.aritulosdisp = JSON.parse(resp.data);
            }
        }, function (error) {
            console.log("error con los articulos");
        });
    };
    ArticulosdisponiblescomponentComponent.prototype.evento = function () {
        var id = parseInt(localStorage.getItem('buttonid'));
        console.log(id);
        this.articuSelec2 = this.aritulosdisp.find(function (art) {
            return art.arIderegistro == id;
        });
    };
    ArticulosdisponiblescomponentComponent.prototype.carga = function () {
        var id = parseInt(localStorage.getItem('buttonid'));
        console.log(id);
        this.articuSelec1 = this.aritulosdisp.find(function (art) {
            return art.arIderegistro == id;
        });
        console.log("secarga el objeto 1");
        console.log(this.articuSelec1);
    };
    ArticulosdisponiblescomponentComponent.prototype.intercambiar = function () {
        console.log(this.articuSelec1);
        console.log(this.articuSelec2);
        var solicitud = new _solicitud__WEBPACK_IMPORTED_MODULE_5__["Solicitud"];
        solicitud.arIdArticuloRemitente = this.articuSelec1;
        solicitud.arIdArticuloDestinatario = this.articuSelec2;
        solicitud.soObservacion = "se le debe la capturada de la obervacion";
        console.log(solicitud);
        this.servicioIntercambio.intercambiar(solicitud).
            subscribe(function (art) {
            console.log(art.id);
            console.log(art.mensaje);
            if (null != art.data && art.id == '200') {
                alert("se proceso de forma correcta");
            }
        }, function (error) {
            console.log("se proceso de forma incorrecta");
        });
    };
    ArticulosdisponiblescomponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-articulosdisponiblescomponent',
            template: __webpack_require__(/*! ./articulosdisponiblescomponent.component.html */ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.html"),
            styles: [__webpack_require__(/*! ./articulosdisponiblescomponent.component.css */ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_3__["ServicioIntercambioService"],
            _usuarioservice_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioserviceService"]])
    ], ArticulosdisponiblescomponentComponent);
    return ArticulosdisponiblescomponentComponent;
}());



/***/ }),

/***/ "./src/app/user/manipula.directive.ts":
/*!********************************************!*\
  !*** ./src/app/user/manipula.directive.ts ***!
  \********************************************/
/*! exports provided: ManipulaDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManipulaDirective", function() { return ManipulaDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ManipulaDirective = /** @class */ (function () {
    function ManipulaDirective(element) {
        this.element = element;
    }
    ManipulaDirective.prototype.ngOnInit = function () {
        this.element.nativeElement.addEventListener('click', this.obtenerid);
    };
    ManipulaDirective.prototype.ngOnDestroy = function () {
        this.element.nativeElement.removeEventListener('click', this.obtenerid);
    };
    ManipulaDirective.prototype.obtenerid = function (id) {
        console.log(id);
        localStorage.setItem('buttonid', id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click', ['$event.target.id']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], ManipulaDirective.prototype, "obtenerid", null);
    ManipulaDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[appManipula]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ManipulaDirective);
    return ManipulaDirective;
}());



/***/ }),

/***/ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/user/misarticuloscomponent/misarticuloscomponent.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li{\r\n\tlist-style: none;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9taXNhcnRpY3Vsb3Njb21wb25lbnQvbWlzYXJ0aWN1bG9zY29tcG9uZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxnQkFBZ0I7QUFDakIiLCJmaWxlIjoic3JjL2FwcC91c2VyL21pc2FydGljdWxvc2NvbXBvbmVudC9taXNhcnRpY3Vsb3Njb21wb25lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpe1xyXG5cdGxpc3Qtc3R5bGU6IG5vbmU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/user/misarticuloscomponent/misarticuloscomponent.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <section class=\"card-columns\" *ngIf=\"articulos.length > 0\">        \r\n    <aside class=\"card\" *ngFor=\"let articulo of articulos\">\r\n            <header class=\"card-header\">\r\n                {{articulo.arNombre}}\r\n                <figure>\r\n                    <img class=\"card-img-top img-fluid\" src={{articulo.arFoto}}>\r\n                </figure>\r\n            </header>\r\n            <div class=\"card-body\">\r\n                <h3 class=\"card-title\">\r\n                    {{articulo.arNombre}}\r\n                </h3>\r\n                <p class=\"card-text\">\r\n                    Tipo articulo\r\n                    {{articulo.arIderegistro}}\r\n                </p>\r\n                <button appManipula (click)=\"carga()\" id={{articulo.arIderegistro}}  class=\"btn btn-primary stretched-link\" data-toggle=\"modal\"\r\n                    data-target=\"#detalle\">\r\n                    Detalle\r\n                </button>\r\n            </div>\r\n    </aside>\r\n    </section>\r\n\r\n    <div class=\"modal bd-example-modal-lg\" id=\"detalle\">\r\n        <div class=\"modal-dialog modal-lg\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <h4 class=\"modal-title\">{{articuSelec1.arNombre}}</h4>\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <br>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-12\" style=\"text-align: center;\" >\r\n                            <figure>\r\n                                <img class=\"img-fluid\" src=\"{{articuSelec1.arFoto}}\">\r\n                                <figcaption>\r\n                                </figcaption>\r\n                            </figure>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-1\"> </div>\r\n                        <div class=\"col-sm-10\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-12\">\r\n                                    <!--Adicionar en este label el resumen del articulo-->\r\n                                    <label id=\"resumen\">\r\n                                        \r\n                                    </label>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-sm-1\"> </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/user/misarticuloscomponent/misarticuloscomponent.component.ts ***!
  \*******************************************************************************/
/*! exports provided: MisarticuloscomponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MisarticuloscomponentComponent", function() { return MisarticuloscomponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _articulo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../articulo */ "./src/app/articulo.ts");
/* harmony import */ var _servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicio-intercambio.service */ "./src/app/servicio-intercambio.service.ts");
/* harmony import */ var _usuarioservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../usuarioservice.service */ "./src/app/usuarioservice.service.ts");
/* harmony import */ var _solicitud__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../solicitud */ "./src/app/solicitud.ts");






var MisarticuloscomponentComponent = /** @class */ (function () {
    function MisarticuloscomponentComponent(servicioIntercambio, servicioUsuario) {
        this.servicioIntercambio = servicioIntercambio;
        this.servicioUsuario = servicioUsuario;
        this.articulos = [];
        this.aritulosdisp = [];
        this.articuSelec1 = new _articulo__WEBPACK_IMPORTED_MODULE_2__["Articulo"];
        this.articuSelec2 = new _articulo__WEBPACK_IMPORTED_MODULE_2__["Articulo"];
    }
    MisarticuloscomponentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.servicioIntercambio.consultaArticulosUsuario(this.servicioUsuario.getUserLoggedIn()).
            subscribe(function (res) {
            if (null != res.data) {
                _this.articulos = JSON.parse(res.data);
            }
        }, function (error) {
            console.log("error con la consulta de usuario");
        });
        this.servicioIntercambio.consultarArticulosDisponibles()
            .subscribe(function (resp) {
            if (null != resp.data) {
                _this.aritulosdisp = JSON.parse(resp.data);
            }
        }, function (error) {
            console.log("error con los articulos");
        });
    };
    MisarticuloscomponentComponent.prototype.evento = function () {
        var id = parseInt(localStorage.getItem('buttonid'));
        console.log(id);
        this.articuSelec2 = this.aritulosdisp.find(function (art) {
            return art.arIderegistro == id;
        });
    };
    MisarticuloscomponentComponent.prototype.carga = function () {
        var id = parseInt(localStorage.getItem('buttonid'));
        console.log(id);
        this.articuSelec1 = this.aritulosdisp.find(function (art) {
            return art.arIderegistro == id;
        });
        console.log("secarga el objeto 1");
        console.log(this.articuSelec1);
    };
    MisarticuloscomponentComponent.prototype.intercambiar = function () {
        console.log(this.articuSelec1);
        console.log(this.articuSelec2);
        var solicitud = new _solicitud__WEBPACK_IMPORTED_MODULE_5__["Solicitud"];
        solicitud.arIdArticuloRemitente = this.articuSelec1;
        solicitud.arIdArticuloDestinatario = this.articuSelec2;
        solicitud.soObservacion = "se le debe la capturada de la obervacion";
        console.log(solicitud);
        this.servicioIntercambio.intercambiar(solicitud).
            subscribe(function (art) {
            console.log(art.id);
            console.log(art.mensaje);
            if (null != art.data && art.id == '200') {
                alert("se proceso de forma correcta");
            }
        }, function (error) {
            console.log("se proceso de forma incorrecta");
        });
    };
    MisarticuloscomponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-misarticuloscomponent',
            template: __webpack_require__(/*! ./misarticuloscomponent.component.html */ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.html"),
            styles: [__webpack_require__(/*! ./misarticuloscomponent.component.css */ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_3__["ServicioIntercambioService"],
            _usuarioservice_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioserviceService"]])
    ], MisarticuloscomponentComponent);
    return MisarticuloscomponentComponent;
}());



/***/ }),

/***/ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/user/missolicitudescomponent/missolicitudescomponent.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container >.row > .cuerpo{\r\n\theight: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9taXNzb2xpY2l0dWRlc2NvbXBvbmVudC9taXNzb2xpY2l0dWRlc2NvbXBvbmVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsWUFBWTtBQUNiIiwiZmlsZSI6InNyYy9hcHAvdXNlci9taXNzb2xpY2l0dWRlc2NvbXBvbmVudC9taXNzb2xpY2l0dWRlc2NvbXBvbmVudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciA+LnJvdyA+IC5jdWVycG97XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/user/missolicitudescomponent/missolicitudescomponent.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container row cuerpo\">\r\n\t<div class=\"col-1\"></div>\r\n<section class=\"col-11\">\r\n\t<h2>Solicitudes</h2>\r\n\t<div *ngIf=\"null != solicitudes\">\r\n\t<div class=\"row\" *ngFor=\"let solicitud of solicitudes; let in=index\" >\r\n\t<aside class=\"card\">\r\n\t\t<header class=\"card-header\">\r\n\t\t\t<h3>codigo de solicitud</h3>\r\n\t\t\t<span>{{solicitud.soCodigo}}</span>\r\n\t\t</header>\r\n\t\t<div class=\"card-body\">\r\n\t\t\t<p> <h4>estado</h4>\r\n\t\t\t\t<span>{{solicitud.soEstado}}</span>\r\n\t\t\t\t<h4>Tipo</h4>\r\n\t\t\t\t<span>{{solicitud.soTipo}}</span>\r\n\t\t\t\t<h4>Observacion</h4>\r\n\t\t\t\t<span>{{solicitud.soObservacion}}</span>\r\n\t\t\t\t<ul>\r\n\t\t\t\t<h4>{{solicitud.arIdArticuloRemitente.arNombre}}</h4>\r\n\t\t\t\t<li>{{solicitud.arIdArticuloRemitente.arNombre}}</li>\r\n\t\t\t\t<li>{{solicitud.arIdArticuloRemitente.arIderegistro}}</li>\r\n\t\t\t\t</ul>\r\n\t\t\t\t\t<ul>\r\n\t\t\t\t<h4>{{solicitud.arIdArticuloDestinatario.arNombre}}</h4>\r\n\t\t\t\t<li>{{solicitud.arIdArticuloDestinatario.arNombre}}</li>\r\n\t\t\t\t<li>{{solicitud.arIdArticuloDestinatario.arIderegistro}}</li>\r\n\t\t\t\t</ul>\r\n\t\t\t<a class=\"btn btn-primary\">Ver Detalles\t</a>\r\n\t\t</div>\r\n\t </aside>\r\n\t</div>\r\n\t</div>\r\n</section>\r\n</div>"

/***/ }),

/***/ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/user/missolicitudescomponent/missolicitudescomponent.component.ts ***!
  \***********************************************************************************/
/*! exports provided: MissolicitudescomponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MissolicitudescomponentComponent", function() { return MissolicitudescomponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicio-intercambio.service */ "./src/app/servicio-intercambio.service.ts");
/* harmony import */ var _usuarioservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../usuarioservice.service */ "./src/app/usuarioservice.service.ts");




var MissolicitudescomponentComponent = /** @class */ (function () {
    function MissolicitudescomponentComponent(servicioUsuario, servicioIntercamb) {
        this.servicioUsuario = servicioUsuario;
        this.servicioIntercamb = servicioIntercamb;
        this.solicitudes = [];
    }
    MissolicitudescomponentComponent.prototype.ngOnInit = function () {
        var _this = this;
        var use = this.servicioUsuario.getUserLoggedIn();
        this.servicioIntercamb.consultarMissolitudes(use).subscribe(function (resp) {
            console.log(resp.mensaje);
            console.log(resp.id);
            if (null != resp.data) {
                _this.solicitudes = JSON.parse(resp.data);
            }
        }, function (error) {
            console.log("Error en el procesamiento");
        });
    };
    MissolicitudescomponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-missolicitudescomponent',
            template: __webpack_require__(/*! ./missolicitudescomponent.component.html */ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.html"),
            styles: [__webpack_require__(/*! ./missolicitudescomponent.component.css */ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_usuarioservice_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioserviceService"],
            _servicio_intercambio_service__WEBPACK_IMPORTED_MODULE_2__["ServicioIntercambioService"]])
    ], MissolicitudescomponentComponent);
    return MissolicitudescomponentComponent;
}());



/***/ }),

/***/ "./src/app/user/perfilcomponent/perfilcomponent.component.css":
/*!********************************************************************!*\
  !*** ./src/app/user/perfilcomponent/perfilcomponent.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcGVyZmlsY29tcG9uZW50L3BlcmZpbGNvbXBvbmVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/user/perfilcomponent/perfilcomponent.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/user/perfilcomponent/perfilcomponent.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n        <div class=\"row\">\r\n        \t<div class=\"col-sm-2\">\r\n            \t<a  class=\"btn btn-info\" routerLink=\"registroarticulo\">Gestion Artículo</a>\r\n            </div>\r\n            <div class=\"col-sm-8\">\r\n                <h2>Mi Perfil</h2>\r\n                <form action=\"\" onsubmit=\"\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-4\">\r\n                            <figure>\r\n                                <img class=\"img-thumbnail\"\r\n                                    src=\"https://img.icons8.com/ios/100/000000/gender-neutral-user-filled.png\" width=\"100\"\r\n                                    height=\"100\">\r\n                                <figcaption>\r\n                                    <label>Nombre Usuario</label>\r\n                                </figcaption>\r\n                            </figure>\r\n                        </div>\r\n                        <div class=\"col-sm-8\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Nombre Usuario\" required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <select class=\"form-control\" id=\"tipoIdentificacion\">\r\n                                            <option>CC</option>\r\n                                            <option>CE</option>\r\n                                            <option>PA</option>\r\n                                            <option>RC</option>\r\n                                            <option>TI</option>\r\n                                        </select>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Número Identificación\"\r\n                                            required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Dirección\" required>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" class=\"form-control\" placeholder=\"Telefono\" required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"password\" class=\"form-control\" placeholder=\"Contraseña\" required>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"password\" class=\"form-control\" placeholder=\"Confirmar Contraseña\"\r\n                                            required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"email\" class=\"form-control\" placeholder=\"Correo electronico\"\r\n                                            required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                    <button type=\"submit\" class=\"btn btn-primary btn-block\">Actualizar</button>\r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                                    <button type=\"submit\" class=\"btn btn-primary btn-block\">Eliminar Cuenta</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </section>\r\n"

/***/ }),

/***/ "./src/app/user/perfilcomponent/perfilcomponent.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/user/perfilcomponent/perfilcomponent.component.ts ***!
  \*******************************************************************/
/*! exports provided: PerfilcomponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilcomponentComponent", function() { return PerfilcomponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PerfilcomponentComponent = /** @class */ (function () {
    function PerfilcomponentComponent() {
    }
    PerfilcomponentComponent.prototype.ngOnInit = function () {
    };
    PerfilcomponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-perfilcomponent',
            template: __webpack_require__(/*! ./perfilcomponent.component.html */ "./src/app/user/perfilcomponent/perfilcomponent.component.html"),
            styles: [__webpack_require__(/*! ./perfilcomponent.component.css */ "./src/app/user/perfilcomponent/perfilcomponent.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PerfilcomponentComponent);
    return PerfilcomponentComponent;
}());



/***/ }),

/***/ "./src/app/user/registroarituc-component/registroarituc-component.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/user/registroarituc-component/registroarituc-component.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcmVnaXN0cm9hcml0dWMtY29tcG9uZW50L3JlZ2lzdHJvYXJpdHVjLWNvbXBvbmVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/user/registroarituc-component/registroarituc-component.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/user/registroarituc-component/registroarituc-component.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-8\">\r\n                <h2>Registro Articulo</h2>\r\n                <form action=\"user\" (ngSubmit)=\"recepcionForm(articuloformulario)\"  #articuloformulario=\"ngForm\" >\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-4\">\r\n                            <figure>\r\n                                <img class=\"img-thumbnail\"\r\n                                    src=\"assets/images/pen_14403.png\" width=\"100\"\r\n                                    height=\"100\">\r\n                                <figcaption>\r\n                                    <label></label>\r\n                                </figcaption>\r\n                            </figure>\r\n                        </div>\r\n                        <div class=\"col-sm-8\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-12\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" [(ngModel)]=\"articulo.arNombre\" class=\"form-control\" name=\"arNombre\" placeholder=\"Nombre Articulo\" required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                     <div class=\"form-group\">\r\n                                        <select name=\"selec\" [(ngModel)]=\"selec\" class=\"form-control\" >\r\n                                            <option *ngFor=\"let opcio of tipos\">{{opcio}}</option>\r\n                                        </select>\r\n                                    </div>\r\n                                   \r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                                    <div class=\"form-group\">\r\n                                        <input type=\"text\" name=\"arFoto\"  [(ngModel)]=\"articulo.arFoto\"         class=\"form-control\" pattern=\"((http|https):\\/\\/(\\w.*))\\.(jpeg|jpg|png|JPG|PNG)\" placeholder=\"http:unaruta/imagen.\"\r\n                                            required>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                            <button type=\"submit\"  [disabled]=\"!articuloformulario.form.valid\"   class=\"btn btn-primary btn-block\">Actualizar</button>\r\n                                </div>\r\n                                <div class=\"col-sm-6\">\r\n                         <button [disabled]=\"!articuloformulario.form.valid\" (click)=\"eliminar=true\"\r\n                                     class=\"btn btn-primary btn-block\">Eliminar Articulo</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"col-sm-2\"></div>\r\n        </div>\r\n    </section>"

/***/ }),

/***/ "./src/app/user/registroarituc-component/registroarituc-component.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/user/registroarituc-component/registroarituc-component.component.ts ***!
  \*************************************************************************************/
/*! exports provided: RegistroaritucComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroaritucComponentComponent", function() { return RegistroaritucComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _usuarioservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../usuarioservice.service */ "./src/app/usuarioservice.service.ts");
/* harmony import */ var _registro_articuloservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../registro-articuloservice.service */ "./src/app/registro-articuloservice.service.ts");
/* harmony import */ var _articulo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../articulo */ "./src/app/articulo.ts");





var RegistroaritucComponentComponent = /** @class */ (function () {
    function RegistroaritucComponentComponent(usuarioServicio, registroArticulo) {
        this.usuarioServicio = usuarioServicio;
        this.registroArticulo = registroArticulo;
        this.articulo = new _articulo__WEBPACK_IMPORTED_MODULE_4__["Articulo"];
        this.selec = "C";
        this.eliminar = false;
    }
    RegistroaritucComponentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.registroArticulo.tipoArticulo().
            subscribe(function (resp) {
            if (resp.id == '200') {
                _this.tipos = JSON.parse(resp.data);
            }
        }, function (error) {
            console.log("ALGO FALLO CONSULTA TIPOS");
        });
    };
    RegistroaritucComponentComponent.prototype.recepcionForm = function (articulo) {
        var _this = this;
        var articuloentrada = {
            arIderegistro: null,
            arNombre: articulo.value.arNombre,
            arTip: articulo.value.selec == "HOGARES" ? 2 : 1,
            arFoto: articulo.value.arFoto,
            cliIdCliente: null,
        };
        if (this.eliminar) {
            var articuloconsul = null;
            this.registroArticulo.buscarArticulo(articuloentrada).
                subscribe(function (resp) {
                alert(resp.mensaje);
                if (null != resp.data) {
                    articuloconsul = JSON.parse(resp.data);
                    _this.registroArticulo.
                        eliminarArticulo(articuloconsul).subscribe(function (res) {
                        alert(res.mensaje);
                    }, function (error) {
                        console.log("ERROR ELIMINANDO");
                    });
                }
            }, function (error) {
                console.log("error en articulo");
            });
        }
        if (!this.eliminar) {
            console.log(articuloentrada);
            var articuloconsul = null;
            articuloentrada.cliIdCliente = this.usuarioServicio.getUserLoggedIn().cliIdecliente;
            console.log(articuloentrada.cliIdCliente);
            this.registroArticulo.buscarArticulo(articuloentrada).
                subscribe(function (resp) {
                if (null != resp.data) {
                    articuloconsul = JSON.parse(resp.data);
                }
            }, function (error) {
                console.log(error);
                console.log("error en articulo");
            });
            if (null != articuloconsul) {
                articuloentrada.arIderegistro = articuloconsul.arIderegistro;
            }
            this.registroArticulo.guardarModificar(articuloentrada).subscribe(function (res) {
                console.log(res.id);
                console.log(res.mensaje);
                alert(res.mensaje);
            }, function (error) {
                console.log("error en el guardar");
            });
        }
        this.eliminar = false;
    };
    RegistroaritucComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registroarituc-component',
            template: __webpack_require__(/*! ./registroarituc-component.component.html */ "./src/app/user/registroarituc-component/registroarituc-component.component.html"),
            styles: [__webpack_require__(/*! ./registroarituc-component.component.css */ "./src/app/user/registroarituc-component/registroarituc-component.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_usuarioservice_service__WEBPACK_IMPORTED_MODULE_2__["UsuarioserviceService"],
            _registro_articuloservice_service__WEBPACK_IMPORTED_MODULE_3__["RegistroArticuloserviceService"]])
    ], RegistroaritucComponentComponent);
    return RegistroaritucComponentComponent;
}());



/***/ }),

/***/ "./src/app/user/user-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/user/user-routing.module.ts ***!
  \*********************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _perfilcomponent_perfilcomponent_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./perfilcomponent/perfilcomponent.component */ "./src/app/user/perfilcomponent/perfilcomponent.component.ts");
/* harmony import */ var _misarticuloscomponent_misarticuloscomponent_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./misarticuloscomponent/misarticuloscomponent.component */ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.ts");
/* harmony import */ var _articulosdisponiblescomponent_articulosdisponiblescomponent_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./articulosdisponiblescomponent/articulosdisponiblescomponent.component */ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.ts");
/* harmony import */ var _missolicitudescomponent_missolicitudescomponent_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./missolicitudescomponent/missolicitudescomponent.component */ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.ts");
/* harmony import */ var _registroarituc_component_registroarituc_component_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./registroarituc-component/registroarituc-component.component */ "./src/app/user/registroarituc-component/registroarituc-component.component.ts");








var routes = [
    {
        path: '',
        component: _perfilcomponent_perfilcomponent_component__WEBPACK_IMPORTED_MODULE_3__["PerfilcomponentComponent"]
    },
    {
        path: 'misarticulos',
        component: _misarticuloscomponent_misarticuloscomponent_component__WEBPACK_IMPORTED_MODULE_4__["MisarticuloscomponentComponent"]
    },
    {
        path: 'articulosdisponibles',
        component: _articulosdisponiblescomponent_articulosdisponiblescomponent_component__WEBPACK_IMPORTED_MODULE_5__["ArticulosdisponiblescomponentComponent"]
    },
    {
        path: 'solicitudes',
        component: _missolicitudescomponent_missolicitudescomponent_component__WEBPACK_IMPORTED_MODULE_6__["MissolicitudescomponentComponent"]
    },
    {
        path: 'registroarticulo',
        component: _registroarituc_component_registroarituc_component_component__WEBPACK_IMPORTED_MODULE_7__["RegistroaritucComponentComponent"]
    },
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "./src/app/user/user.module.ts":
/*!*************************************!*\
  !*** ./src/app/user/user.module.ts ***!
  \*************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _user_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-routing.module */ "./src/app/user/user-routing.module.ts");
/* harmony import */ var _misarticuloscomponent_misarticuloscomponent_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./misarticuloscomponent/misarticuloscomponent.component */ "./src/app/user/misarticuloscomponent/misarticuloscomponent.component.ts");
/* harmony import */ var _articulosdisponiblescomponent_articulosdisponiblescomponent_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./articulosdisponiblescomponent/articulosdisponiblescomponent.component */ "./src/app/user/articulosdisponiblescomponent/articulosdisponiblescomponent.component.ts");
/* harmony import */ var _missolicitudescomponent_missolicitudescomponent_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./missolicitudescomponent/missolicitudescomponent.component */ "./src/app/user/missolicitudescomponent/missolicitudescomponent.component.ts");
/* harmony import */ var _perfilcomponent_perfilcomponent_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./perfilcomponent/perfilcomponent.component */ "./src/app/user/perfilcomponent/perfilcomponent.component.ts");
/* harmony import */ var _registroarituc_component_registroarituc_component_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./registroarituc-component/registroarituc-component.component */ "./src/app/user/registroarituc-component/registroarituc-component.component.ts");
/* harmony import */ var _manipula_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./manipula.directive */ "./src/app/user/manipula.directive.ts");











var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_misarticuloscomponent_misarticuloscomponent_component__WEBPACK_IMPORTED_MODULE_5__["MisarticuloscomponentComponent"], _articulosdisponiblescomponent_articulosdisponiblescomponent_component__WEBPACK_IMPORTED_MODULE_6__["ArticulosdisponiblescomponentComponent"], _missolicitudescomponent_missolicitudescomponent_component__WEBPACK_IMPORTED_MODULE_7__["MissolicitudescomponentComponent"], _perfilcomponent_perfilcomponent_component__WEBPACK_IMPORTED_MODULE_8__["PerfilcomponentComponent"], _registroarituc_component_registroarituc_component_component__WEBPACK_IMPORTED_MODULE_9__["RegistroaritucComponentComponent"], _manipula_directive__WEBPACK_IMPORTED_MODULE_10__["ManipulaDirective"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _user_routing_module__WEBPACK_IMPORTED_MODULE_4__["UserRoutingModule"],
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ })

}]);
//# sourceMappingURL=user-user-module.js.map