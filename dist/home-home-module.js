(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _quienesomos_component_quienesomos_component_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./quienesomos-component/quienesomos-component.component */ "./src/app/home/quienesomos-component/quienesomos-component.component.ts");
/* harmony import */ var _index_component_index_component_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./index-component/index-component.component */ "./src/app/home/index-component/index-component.component.ts");





var routes = [
    { path: 'quienes-somos',
        component: _quienesomos_component_quienesomos_component_component__WEBPACK_IMPORTED_MODULE_3__["QuienesomosComponentComponent"]
    },
    {
        path: '',
        component: _index_component_index_component_component__WEBPACK_IMPORTED_MODULE_4__["IndexComponentComponent"]
    },
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _quienesomos_component_quienesomos_component_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./quienesomos-component/quienesomos-component.component */ "./src/app/home/quienesomos-component/quienesomos-component.component.ts");
/* harmony import */ var _index_component_index_component_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./index-component/index-component.component */ "./src/app/home/index-component/index-component.component.ts");






var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_quienesomos_component_quienesomos_component_component__WEBPACK_IMPORTED_MODULE_4__["QuienesomosComponentComponent"], _index_component_index_component_component__WEBPACK_IMPORTED_MODULE_5__["IndexComponentComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_3__["HomeRoutingModule"]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/home/index-component/index-component.component.css":
/*!********************************************************************!*\
  !*** ./src/app/home/index-component/index-component.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaW5kZXgtY29tcG9uZW50L2luZGV4LWNvbXBvbmVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/index-component/index-component.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/home/index-component/index-component.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <div class=\"container\">\r\n        <section id=\"carouselprin\" class=\"carousel slide\" data-ride=\"carousel\">\r\n            <div class=\"carousel-inner\">\r\n            <figure class=\"carousel-item active\">\r\n                <img class=\"d-block img-fluid\" src=\"http://farm3.static.flickr.com/2103/2285949233_c0c6afc44c.jpg\">\r\n            </figure>\r\n            <figure class=\"carousel-item\">\r\n                <img class=\"d-block\" src=\"http://farm1.static.flickr.com/84/265500799_0a6d1a9c23.jpg\" >\r\n            </figure>\r\n            <figure class=\"carousel-item\">\r\n                <img class=\"d-block img-fluid\" src=\"http://farm2.static.flickr.com/1304/530133524_7e813c709e.jpg\">\r\n            </figure>\r\n            <figure class=\"carousel-item\">\r\n                <img class=\"d-block img-fluid\" src=\"http://farm4.static.flickr.com/3524/3266290971_9041c18bb2.jpg\">\r\n            </figure>\r\n          </div>\r\n        </section>\r\n            <hr>\r\n        <section class=\"card-columns\">\r\n            <aside class=\"card\">\r\n                    <header class=\"card-header\">\r\n                        Aritculo Especial\r\n                        <figure>\r\n                        <img class=\"card-img-top img-fluid\" \r\n                             src=\"http://farm2.static.flickr.com/1304/530133524_7e813c709e.jpg\">\r\n                        </figure>\r\n                    </header>\r\n                    <div class=\"card-body\">\r\n                        <h3 class=\"card-title\">\r\n                            Arituclo Uno\r\n                        </h3>\r\n                        <p class=\"card-text\">\r\n                          Lorem Ipsum is simply dummy text of the printing and \r\n                          typesetting industry        \r\n                        </p>\r\n                    </div>\r\n            </aside>\r\n            <aside class=\"card\">\r\n                    <header class=\"card-header\">\r\n                        Aritculo Especial\r\n                        <figure>\r\n                        <img class=\"card-img-top img-fluid\" \r\n                             src=\"http://farm4.static.flickr.com/3524/3266290971_9041c18bb2.jpg\">\r\n                        </figure>\r\n                    </header>\r\n                    <div class=\"card-body\">\r\n                        <h3 class=\"card-title\">\r\n                            Arituclo Dos\r\n                        </h3>\r\n                        <p class=\"card-text\">\r\n                          Lorem Ipsum is simply dummy text of the printing and \r\n                          typesetting industry         \r\n                        </p>\r\n                    </div>\r\n            </aside>\r\n            <aside class=\"card\">\r\n                    <header class=\"card-header\">\r\n                        Aritculo Especial\r\n                        <figure>\r\n                        <img class=\"card-img-top img-fluid\" \r\n                             src=\"http://farm1.static.flickr.com/84/265500799_0a6d1a9c23.jpg\">\r\n                        </figure>\r\n                    </header>\r\n                    <div class=\"card-body\">\r\n                        <h3 class=\"card-title\">\r\n                            Arituclo Tres\r\n                        </h3>\r\n                        <p class=\"card-text\">\r\n                          Lorem Ipsum is simply dummy text of the printing and\r\n                          typesetting industry. Lorem Ipsum has been the        \r\n                        </p>\r\n                    </div>\r\n            </aside>\r\n        </section>\r\n       </div>\r\n"

/***/ }),

/***/ "./src/app/home/index-component/index-component.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/home/index-component/index-component.component.ts ***!
  \*******************************************************************/
/*! exports provided: IndexComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexComponentComponent", function() { return IndexComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var IndexComponentComponent = /** @class */ (function () {
    function IndexComponentComponent() {
    }
    IndexComponentComponent.prototype.ngOnInit = function () {
    };
    IndexComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-index-component',
            template: __webpack_require__(/*! ./index-component.component.html */ "./src/app/home/index-component/index-component.component.html"),
            styles: [__webpack_require__(/*! ./index-component.component.css */ "./src/app/home/index-component/index-component.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], IndexComponentComponent);
    return IndexComponentComponent;
}());



/***/ }),

/***/ "./src/app/home/quienesomos-component/quienesomos-component.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/home/quienesomos-component/quienesomos-component.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvcXVpZW5lc29tb3MtY29tcG9uZW50L3F1aWVuZXNvbW9zLWNvbXBvbmVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/quienesomos-component/quienesomos-component.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/home/quienesomos-component/quienesomos-component.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <!--https://sp.depositphotos.com/102194540/stock-photo-who-we-are-question-doodle.html-->\r\n                <img class=\"img-fluid mx-auto d-block\" width=\"200px\" src=\"assets/images/quienes_somos.jpg\">\r\n            </div>\r\n        </div>\r\n        <hr>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n                <img class=\"img-responsive mx-auto d-block\" height=\"200px\" src=\"assets/images/quienes_somos_body.png\">\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n                <p class=\"text-justify\">\r\n                    Articulo App es una aplicación web para intercambiar artículos entre personas, \r\n                    ABCSoftware es la empresa encargada del desarrollo y manotenci&oacute;n del software,\r\n                    Nos encargamos de la venta desarrollo y creaci&oacute;n de software para empresas,\r\n                    organizaciones gobernamentales etc...\r\n                </p>\r\n            </div>\r\n            <div class=\"col-sm-2\"></div>\r\n        </div>\r\n    </section>\r\n"

/***/ }),

/***/ "./src/app/home/quienesomos-component/quienesomos-component.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/home/quienesomos-component/quienesomos-component.component.ts ***!
  \*******************************************************************************/
/*! exports provided: QuienesomosComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuienesomosComponentComponent", function() { return QuienesomosComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuienesomosComponentComponent = /** @class */ (function () {
    function QuienesomosComponentComponent() {
    }
    QuienesomosComponentComponent.prototype.ngOnInit = function () {
    };
    QuienesomosComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-quienesomos-component',
            template: __webpack_require__(/*! ./quienesomos-component.component.html */ "./src/app/home/quienesomos-component/quienesomos-component.component.html"),
            styles: [__webpack_require__(/*! ./quienesomos-component.component.css */ "./src/app/home/quienesomos-component/quienesomos-component.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuienesomosComponentComponent);
    return QuienesomosComponentComponent;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map